from flask import Flask
from flask_restful import Resource, Api

application = Flask(__name__)

api = Api(application)

class HealthCheck(Resource):
    def get(self):
        return 'OK'

api.add_resource(HealthCheck, '/ws/healthz/')

INFO = {
    "id": "poisonousanimals",
    "displayName": "Poison Animals",
    "center": {"latitude": "0", "longitude": "0"},
    "zoom":1
}

class Info(Resource):
    def get(self):
        return INFO

api.add_resource(Info, '/ws/info/')

DATA = [
    {
        "id": "animal1",
        "name": "Crocodile",
        "position": {"latitude": "0", "longitude": "0"},
        "info": "Kills you by chewing you up."
    },
    {
        "id": "animal2",
        "name": "Funnel Web",
        "position": {"latitude": "5", "longitude": "5"},
        "info": "Kills you with venom."
    }
]

class DataAll(Resource):
    def get(self):
        return DATA

api.add_resource(DataAll, '/ws/data/all')

class DataWithin(Resource):
    def get(self):
        return DATA

api.add_resource(DataWithin, '/ws/data/within')
